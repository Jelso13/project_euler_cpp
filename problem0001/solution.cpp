#include <bits/stdc++.h>

using namespace std;

int SumDivisibleBy(int n) {
    int p = 1000 / n;
    return n * (p * (p + 1)) / 2;
}

int main() {
    int sum = 0;
    sum = SumDivisibleBy(3) + SumDivisibleBy(5) - SumDivisibleBy(15);
    cout << sum << "\n";
    return 0;
}
