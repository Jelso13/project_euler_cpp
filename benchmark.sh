#!/bin/sh

# get the first argument and pad left with 0 to length 4
FOLDER="problem$(printf "%04d" $1)"

cd $FOLDER

# for each cpp file in the folder
# compile and run it

for file in $(ls *.cpp); do
    file_name=$(echo $file | cut -f 1 -d '.')
    echo "---------------"
    echo $file_name
    # get the file name without extension

    g++ $file -o $file_name
    start=$(date +%s.%N)
    
    time ./$file_name
    
    # This is not accurate enough
    # printf "Execution time: %.6f seconds \n" $(echo "$(date +%s.%N) - $start" | bc)


done
