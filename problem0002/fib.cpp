#include <bits/stdc++.h>


using namespace std;

int main() {

    int prev = 1;
    int cur = 1;
    int tmp = 1;
    int sum = 0;

    while (cur < 4000000) {
        tmp = cur;
        cur = prev + cur;
        prev = tmp;
        if (cur % 2 == 0) {
            sum += cur;
        }
    }

    cout << sum << "\n";

    return 0;
}
